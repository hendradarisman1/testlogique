<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Register &mdash; Stisla</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.css">

  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/components.css">
</head>

<body>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
            <div class="login-brand">
              <img src="<?php echo base_url();?>assets/img/stisla-fill.svg" alt="logo" width="100" class="shadow-light rounded-circle">
            </div>
            <?php
		    	if(validation_errors()){
		    		?>
		    		<div class="alert alert-info text-center">
		    			<?php echo validation_errors(); ?>
		    		</div>
		    		<?php
		    	}
 
				if($this->session->flashdata('message')){
					?>
					<div class="alert alert-info text-center">
						<?php echo $this->session->flashdata('message'); ?>
					</div>
					<?php
				}	
		    ?>
            <div class="card card-primary">
              <div class="card-header"><h4>Register</h4></div>

              <div class="card-body">
              <form method="POST" action="<?php echo base_url().'index.php/user/update'; ?>">
              <input type="hidden" name="id" id="id" value="<?php echo $details[0]->id; ?>">
                  <div class="row">
                    <div class="form-group col-6">
                      <label for="frist_name">First Name</label>
                      <input id="first_name" type="text" class="form-control" name="first_name" value="<?php echo $details[0]->first_name;?>" >
                    </div>
                    <div class="form-group col-6">
                      <label for="last_name">Last Name</label>
                      <input id="last_name" type="text" class="form-control" name="last_name" value="<?php echo $details[0]->last_name;?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="form-control" name="email" value="<?php echo $details[0]->email;?>">
                    <div class="invalid-feedback">
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group col-6">
                      <label for="password" class="d-block">Password</label>
                      <input id="password" type="password" class="form-control"  name="password" value="<?php echo $details[0]->password;?>">
                     </div>
                    
                    <div class="form-group col-6">
                      <label for="password2" class="d-block">Password Confirmation</label>
                      <input id="password_confirm" type="password" class="form-control" name="password_confirm" value="<?php echo $details[0]->password;?>">
                    </div>

                
                  </div>

                  <div class="form-divider">
                    Your Detail Profile
                  </div>

                  <div class="row">

                  <div class="form-group  col-8">
                    <label for="address">Address</label>
                    <textarea class="form-control" id="address" name="address" rows="20"><?php echo $details[0]->address;?></textarea>
                  </div>
                  </div>

                  <div class="row">
                    <div class="form-group col-6">
                      <label>Birthday</label>
                      <input type="text" class="form-control datepicker" id="birthday" name="birthday" value="<?php echo $details[0]->birthday;?>">
                    </div>
                    
                    <div class="form-group col-6">
                      <label>Member Type</label>
                      <select name="member" id="member" class="form-control selectric">
                      <option value="<?php echo $details[0]->member;?>"><?php echo $details[0]->member;?></option>';

                      <option value="Silver">Silver</option>
                      <option value="Gold">Gold</option>
                      <option value="Platinum">Platinum</option>
                      <option value="Black">Black</option>
                      <option value="VIP">VIP</option>
                      <option value="VVIP">VVIP</option>
                      
                      </select>
                    </div>
                  </div>
                  <div class="form-divider">
                   Your Credit Card
                  </div>
                  <div class="row">
                  <div class="form-group col-6">
                      <label for="card_number">Card Number</label>
                      <input type="text" class="form-control creditcard" id="card_number" name="card_number" required value="<?php echo $details[0]->cc_num;?>">
                    </div>

                    <div class="form-group col-3">
                      <label for="exp_date">Exp Date</label>
                      <input id="exp_date" type="text" class="form-control exp_date" name="exp_date" placeholder="MM / YYYYY" required value="<?php echo $details[0]->exp_num;?>">
                    </div>

                    <div class="form-group col-3">
                      <label for="cvv">CVV</label>
                      <input id="cvv" type="text" class="form-control" name="cvv" maxlength="3" required value="<?php echo $details[0]->cvv;?>">
                    </div>

                    </div>

            
                  

                
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                      Update Profile
                    </button>
                  </div>
                </form>
              </div>
            </div>
            <div class="simple-footer">
              Copyright &copy; Stisla 2018
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  
  <script src="https://demo.getstisla.com/assets/modules/tooltip.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/stisla.js"></script>

  <script src="https://demo.getstisla.com/assets/modules/cleave-js/dist/cleave.min.js"></script>
  <script src="https://demo.getstisla.com/assets/modules/cleave-js/dist/addons/cleave-phone.us.js"></script>

  
  <!-- JS Libraies -->
  <script src="https://demo.getstisla.com/assets/modules/jquery-pwstrength/jquery.pwstrength.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.js"></script>

  <!-- Template JS File -->
  <script src="<?php echo base_url();?>assets/js/scripts.js"></script>
  <script src="<?php echo base_url();?>assets/js/custom.js"></script>
   <!-- Page Specific JS File -->
 

  <!-- Page Specific JS File -->
  <script src="<?php echo base_url();?>assets/js/page/auth-register.js"></script>
  <script src="<?php echo base_url();?>/assets/js/page/forms-advanced-forms.js"></script>
  

</body>
</html>
