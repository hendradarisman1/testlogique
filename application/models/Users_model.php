<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function getAllUsers(){
		$query = $this->db->get('users');
		return $query->result(); 
	}

	public function insert($user){
		$this->db->insert('users', $user);
		return $this->db->insert_id(); 
    }
    
    public function insert2($detail){
		$this->db->insert('user_detail', $detail);
		return $this->db->insert_id(); 
	}

    public function update($id,$update){
       
        $this->db->where('id', $id);
		return $this->db->update('users', $update);
    }

	public function getUser($id){
		$query = $this->db->get_where('users',array('id'=>$id));
		return $query->row_array();
	}

	public function activate($data, $id){
		$this->db->where('users.id', $id);
		return $this->db->update('users', $data);
    }
    
    public function get_user($email, $pwd)
	{
		$this->db->where('email', $email);
        $this->db->where('password', $pwd);
        $this->db->where('active','1');
        $query = $this->db->get('users');
		return $query->result();
	}
	
	// get user
	public function get_user_by_id($id)
	{
		$this->db->where('id', $id);
        $query = $this->db->get('users');
		return $query->result();
	}

}
