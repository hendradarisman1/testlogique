<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public $data;

	public function __construct() {
		
		parent::__construct();
	    $this->load->model('users_model');
		$this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');

		
	}
	
	
	public function index() {
		
		
    }
    
	public function register() {
        $data = $this->data;
        $data['title']        = "Form Register";
        // $this->form_validation->set_rules('email', 'Email', 'trim|required|alpha_numeric|min_length[4]|is_unique[users.username]', array('is_unique' => 'This username already exists. Please choose another one.'));
		$this->form_validation->set_rules('email', 'Email', 'valid_email|required');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[7]|max_length[30]');
        $this->form_validation->set_rules('password_confirm', 'Confirm Password', 'required|matches[password]');

        if ($this->form_validation->run() == FALSE) { 
             $this->load->view('main/register', $data);
		}
		else{
			//get user inputs
			$email    = $this->input->post('email');
            $password = $this->input->post('password');
            $first    = $this->input->post('first_name');
            $last     = $this->input->post('last_name');

			//generate simple random code
			$set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$code = substr(str_shuffle($set), 0, 12);

			//insert user to users table and get id
			$user['email']      = $email;
			$user['password']   = $password;
			$user['code']       = $code;
            $user['active']     = false;
            $user['first_name'] = $first;
            $user['last_name']  = $last;
            $id = $this->users_model->insert($user);

            // $detail = array('first_name'=>  $first,
            //                 'last_name' =>  $last,
            //                 'id_user'   =>  $id
            //                 );
            // $insert = $this->users_model->insert2($detail);
            

			//set up email
			$config = array(
		  		'protocol'      => 'smtp',
		  		'smtp_host'     => 'ssl://smtp.gmail.com',
		  		'smtp_port'     => 465,
		  		'smtp_user'     => 'darismanhendra@gmail.com', // change it to yours
		  		'smtp_pass'     => 'darisman94', // change it to yours
		  		'smtp_username' => 'darismanhendra@gmail.com',
		  		'mailtype'      => 'html',
		  		'charset'       => 'iso-8859-1',
		  		'wordwrap'      => TRUE
			);

			$message = 	"
						<html>
						<head>
							<title>Verification Code</title>
						</head>
						<body>
							<h2>Thank you for Registering.</h2>
							<p>Your Account:</p>
							<p>Email: ".$email."</p>
							<p>Password: ".$password."</p>
							<p>Please click the link below to activate your account.</p>
							<h4><a href='".base_url()."index.php/user/activate/".$id."/".$code."'>Activate My Account</a></h4>
						</body>
						</html>
						";
	 		
		    $this->load->library('email', $config);
		    $this->email->set_newline("\r\n");
		    $this->email->from($config['smtp_user']);
		    $this->email->to($email);
		    $this->email->subject('Signup Verification Email');
		    $this->email->message($message);

		    //sending email
		    if($this->email->send()){
		    	$this->session->set_flashdata('message','Activation code sent to email');
		    }
		    else{
		    	$this->session->set_flashdata('message', $this->email->print_debugger());
	 
		    }

            redirect('index.php/user/register');
          }
		
    }
    
    public function activate(){
		$id =  $this->uri->segment(3);
		$code = $this->uri->segment(4);
    	//fetch user details
		$user = $this->users_model->getUser($id);

		//if code matches
		if($user['code'] == $code){
			//update user active status
			$data['active'] = true;
			$query = $this->users_model->activate($data, $id);

			if($query){
				$this->session->set_flashdata('message', 'User activated successfully');
			}
			else{
				$this->session->set_flashdata('message', 'Something went wrong in activating account');
			}
		}
		else{
			$this->session->set_flashdata('message', 'Cannot activate account. Code didnt match');
		}

		redirect('index.php/user/register');

	}

//Function Login
    public function login() {
		 
        $data['title'] = 'Login';
		
		// set validation rules
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if ($this->form_validation->run() == false) {
			
			// validation not ok, send validation errors to the view
			$this->load->view('main/login');
			
		} else {
			
			// set variables from the form
			$email = $this->input->post('email');
            $password = $this->input->post('password');
            

            $result = $this->users_model->get_user($email, $password);
          
           if (count($result) > 0)
			{
				// set session
				$sess_data = array(
                    'login' => TRUE,
                    'id'    => $result[0]->id,
                    'email' => $result[0]->email
                );
                $this->session->set_userdata($sess_data);
            
				redirect("index.php/user/view");
			}
			else
			{
                $this->session->set_flashdata('message', 'salah username atau password');
				
            }
            redirect('index.php/user/login');
			
		}
		
	}
    
    public function view(){

        $data['details']        = $this->users_model->get_user_by_id($this->session->userdata('id'));       
		$this->load->view('main/view', $data);
    }

    public function update(){
        $id       = $this->input->post('id');
        $email    = $this->input->post('email');
        $password = $this->input->post('password');
        $first    = $this->input->post('first_name');
        $last     = $this->input->post('last_name');
        $address  = $this->input->post('address'); 
        $birthday = $this->input->post('birthday');
        $member   = $this->input->post('member');
        $cc_num   = $this->input->post('card_number');
        $exp_num  = $this->input->post('exp_date');
        $cvv      = $this->input->post('cvv');
       
        

        $update = array(
                        'email'      => $email,
                        'password'   => $password,
                        'first_name' => $first,
                        'last_name'  => $last,
                        'address'    => $address,
                        'birthday'   => $birthday,
                        'member'     => $member,
                        'cc_num'     => $cc_num,
                        'exp_num'    => $exp_num,
                        'cvv'        => $cvv
                        );

        $up = $this->users_model->update($id,$update);
        if($up){
            $this->session->set_flashdata('message', 'Update Profile Berhasil');
        }
        else{
            $this->session->set_flashdata('message', 'Something went wrong contact administrator');
        }
        redirect('index.php/user/view');

    }
    public function logout() {
		
		// destroy session
        $data = array('login' => '', 'email' => '', 'id' => '');
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
		redirect('home/index');
		
    }
    
    
	
}