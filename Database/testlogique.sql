/*
Navicat MySQL Data Transfer

Source Server         : SQL
Source Server Version : 100119
Source Host           : localhost:3306
Source Database       : testlogique

Target Server Type    : MYSQL
Target Server Version : 100119
File Encoding         : 65001

Date: 2020-01-14 14:55:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `id_member` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_member`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of member
-- ----------------------------
INSERT INTO `member` VALUES ('1', 'Silver');
INSERT INTO `member` VALUES ('2', 'Gold');
INSERT INTO `member` VALUES ('3', 'Platinum');
INSERT INTO `member` VALUES ('4', 'Black');
INSERT INTO `member` VALUES ('5', 'VIP');
INSERT INTO `member` VALUES ('6', 'VVIP');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `code` varchar(20) NOT NULL,
  `active` int(1) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `member` varchar(10) DEFAULT NULL,
  `cc_num` varchar(18) DEFAULT NULL,
  `exp_num` varchar(6) DEFAULT NULL,
  `cvv` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('5', 'hendradarisman34@gmail.com', '1234567', 'DMQKTrg25FpJ', '1', 'Hendra', 'Darisman', 'Bandung', '2020-01-14', 'Gold', '5421 3213 2312 124', '12/50', '512');
SET FOREIGN_KEY_CHECKS=1;
