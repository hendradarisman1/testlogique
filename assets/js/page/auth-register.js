"use strict";

$('.email input:first').on('keyup', function(){
    var valid = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(this.value) && this.value.length;
    console.log(valid);
    $('#invalid-feedback').html('It\'s'+ (valid?'':' not') +' valid');
});