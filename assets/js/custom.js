/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 *
 */

"use strict";
$('.email input:first').on('keyup', function(){
    var valid = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(this.value) && this.value.length;
    console.log(valid);
    $('#invalid-feedback').html('It\'s'+ (valid?'':' not') +' valid');
});