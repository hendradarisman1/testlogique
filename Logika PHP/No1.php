//Fungsi Pengecekan sebuah bilangan prima

<?php
 
 
function BilanganPrima($nomor)
{
   
    if ( $nomor == 1 ) {
        return false;
    }
  
    if ( $nomor == 2 ) {
        return true;
    }
   
    $x = sqrt($nomor);
    $x = floor($x);
   
    for ( $i = 2 ; $i <= $x ; ++$i ) {
        if ( $nomor % $i == 0 ) {
            break;
        }
    }
     
  if( $x == $i-1 ) {
        return true;
    } else {
        return false;
    }
}

function Check($status){
    if($status == true) {
        return "Ya Bilangan Prima";
      } else { 
        return "Bukan Bilangan Prima";
      }
}
 
    for ($i=101; $i<=120; $i++)
        { 
        echo  $i. " Bilangan prima adalah  ".Check(BilanganPrima($i))."";
        }

?>

//Penjelasan
// 1 adalah posisi awal bukan bilangan prima
// Fungsi sqrt digunakan untuk menghitung akar pangkat dua atau akar kuadrat dari sebuah angka yg di input
// Fungsi floor digunakan untuk membulatkan angka hasil ke nilai terbawah, round adalah ke atas
